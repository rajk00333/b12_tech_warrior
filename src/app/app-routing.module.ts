import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginPageComponent} from './login-page/login-page.component';
import { AddAssetPageComponent} from './add-asset-page/add-asset-page.component';
import { OverViewPageComponent} from './over-view-page/over-view-page.component'
import { StockPageComponent} from './stock-page/stock-page.component';
import {AllocateAssetPageComponent} from './allocate-asset-page/allocate-asset-page.component';

const routes: Routes = [
  {path: '', pathMatch:'full', redirectTo:'login'},
  {path: 'login', component: LoginPageComponent},
  {path: 'home', component: OverViewPageComponent},
  {path: 'stockPage', component: StockPageComponent},
  {path: 'addAssetPage', component: AddAssetPageComponent},
  {path: 'allocateAsset', component: AllocateAssetPageComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}