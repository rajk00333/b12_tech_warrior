import { Component, OnInit } from '@angular/core';
import { AllocateAsset, OverviewAllocatedAssets } from '../shared/models/allocateAsset.model';
import { Router } from '@angular/router';
import { AssetService } from '../shared/services/asset.service';
import { UserService } from '../shared/services/user.service';
import { UserDetailService } from '../shared/services/user-detail.service';
import { UserDetails } from '../shared/models/user-details.model';
import { Asset } from '../shared/models/Asset.model';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { allocateAssetService } from '../shared/services/allocateAsset';

@Component({
  selector: 'app-allocate-asset-page',
  templateUrl: './allocate-asset-page.component.html',
  styleUrls: ['./allocate-asset-page.component.css']
})
export class AllocateAssetPageComponent implements OnInit {

  allocatedAsset: AllocateAsset = new AllocateAsset();
  userDetails: UserDetails[];
  user: UserDetails = new UserDetails();
  asset: Asset = new Asset();
  private AssetService: AssetService;
  private UserDetailService: UserDetailService;
  assets: Asset[];
  public  access = [
    {id: '1', value :"AWS"},
    {id: '2', value :"BitBucket"},
    {id: '3', value :"Jira"},
    {id: '4', value :""},
  ];

  constructor(
    private router: Router,
    private assetService: AssetService,
    private userService: UserService,
    private _UserDetailService: UserDetailService,
    private _AssetService: AssetService,
    private _allocateAssetService: allocateAssetService,
    private toastr: ToastrService) {
    this.UserDetailService = _UserDetailService;
    this.AssetService = _AssetService;
  }

  onSubmit(form: NgForm) {
    let data = form.value;
    if (data.tillDate >= data.fromDate) {

      let filteredData = {
        fromDate: data.fromDate, purpose: data.purpose,
        quantity: data.quantity, tillDate: data.tillDate, userId: this.user.id, assetId: this.asset.id,
        accessAllocation: data.accessAllocation
      }

      this._allocateAssetService.addAllocatedAsset(filteredData).then(() => {
        this.toastr.success("successfully allocated");
        this.router.navigate(['home']);
      })
    }
    else {
      this.toastr.error("Till Date should be same or greater than From Date");
    }
  }
  ngOnInit() {
    this.allocatedAsset = new AllocateAsset();
    this.resetFields();

    this.UserDetailService.getUserDetails().subscribe(userDetails => {
      this.userDetails = userDetails.map(userDetail => {
        return {
          id: userDetail.payload.doc.id,
          ...userDetail.payload.doc.data()
        } as UserDetails
      });
      console.log(this.userDetails);
    });

    this.AssetService.getAssets().subscribe(assets => {
      this.assets = assets.map(asset => {
        return {
          id: asset.payload.doc.id,
          ...asset.payload.doc.data()
        } as Asset
      });
    });
  }

  resetFields() {
    this.asset.serialNumber = "";
    this.asset.name = "";
    this.user.userName = "";
    this.user.userEmail = "";
    this.user.phoneNumber = null;
    this.allocatedAsset.fromDate = null;
    this.allocatedAsset.tillDate = null;
    this.allocatedAsset.quantity = null;
    this.allocatedAsset.purpose = "";
    this.allocatedAsset.accessAllocation = "";

  }

  userSelection(id) {
    var found: UserDetails[] = this.userDetails.filter(c => c.id == id);
    if (found) {
      this.user = found[0];
    }
  }

  assetSelection(id) {
    var found: Asset[] = this.assets.filter(c => c.id == id);
    if (found) {
      this.asset = found[0];
    }
  }
}



