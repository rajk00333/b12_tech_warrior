import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

//firebase
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';

import { UserService } from '../shared/services/user.service';
import { User } from '../shared/models/User.model';
import { NgForOf } from '@angular/common';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  user: User;
  message: string;
  loading: Boolean = false;

  constructor
 (
        private router: Router,
        private userService : UserService,
        public afAuth: AngularFireAuth
  //    private loginService: LoginService,
  //    private loginAuthGuard: LoginAuthGuardService
 ) {}

  ngOnInit() {
     this.user = new User();
     this.user.email = "Raj.kumar@itt.com";
     this.user.password = "rajkumar123";
     this.user = {email :"", password:""};    
  };

  onClickLoginButton(){
    this.userService.addUser(this.user);
    alert("logged-in successfully");
    console.log(this.user);
  }

  onSignIn(){
    this.message = undefined;
    this.loading = true;
    const obj = {'username': this.user.email, 'password':this.user.password}
    //console.log(obj);
    // this.router.navigate(['home']);
  }
  loginSubmit(loginCredentails: NgForm){
    this.user = loginCredentails.value as User;
    // Todo move the below code to auth service
    firebase.auth().signInWithEmailAndPassword(this.user.email,this.user.password)
      .then(res => {
        console.log(res);
        this.router.navigate(['home']);
      }, err => alert("Wrong User id or password") )
  }
}