import { Component, OnInit } from '@angular/core';
import { AssetService } from '../shared/services/asset.service';
import { Asset } from '../shared/models/Asset.model';
import { Router } from '@angular/router';

@Component({
  selector: 'stockPage',
  templateUrl: './stock-page.component.html',
  styleUrls: ['./stock-page.component.css']
})
export class StockPageComponent implements OnInit {

  private AssetService : AssetService;
  assets : Asset[];

  constructor( _AssetService: AssetService,
    private router: Router) { 
    this.AssetService = _AssetService;
  }
  
  ngOnInit() {
    this.AssetService.getAssets().subscribe(assets => {
      this.assets = assets.map(asset => {
        return {
          id: asset.payload.doc.id,
          ...asset.payload.doc.data()
        } as Asset
      });
    });
    console.log(Asset);
  }
  onEdit(asset : Asset) {
   this.router.navigate(['addAssetPage'], { queryParams : {'assetId': asset.id}} );
  }
  OnDelete(id: string){
    if(confirm("Are you sure to DELETE this record")){
    this.AssetService.onDelete(id);
  }
  }
}
