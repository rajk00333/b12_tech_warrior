import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire'
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AddAssetPageComponent } from './add-asset-page/add-asset-page.component';
import { AllocateAssetPageComponent } from './allocate-asset-page/allocate-asset-page.component';
import { OverViewPageComponent } from './over-view-page/over-view-page.component';
import { StockPageComponent } from './stock-page/stock-page.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { AssetService } from './shared/services/asset.service';
import { environment } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr'; 
import { UserDetailService } from './shared/services/user-detail.service';
import { Ng2SearchPipeModule} from 'ng2-search-filter';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    AddAssetPageComponent,
    AllocateAssetPageComponent,
    OverViewPageComponent,
    StockPageComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxPaginationModule,
    Ng2SearchPipeModule
  ],
  providers: [AssetService, UserDetailService],
  bootstrap: [AppComponent]
})
export class AppModule { 

  public navigationBar : Boolean;
}
