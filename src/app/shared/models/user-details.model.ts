export class UserDetails {
  public id: string;
  public userEmail: string;
  public userName: string;
  public phoneNumber: number;
}
