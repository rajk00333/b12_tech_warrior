export class Asset{
    public id: string;
    public name: string;
    public serialNumber: string;
    public quantity: number;
    public price: number;
    public details: string;
    public company: string;
}  