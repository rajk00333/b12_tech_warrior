import { Component, OnInit } from '@angular/core';
import { AssetService } from '../services/asset.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    addassetButton= "Add asset";
    allocateassetButton= "Allocate Asset and Access";
    overView = "Overview";
    stocks= "Stocks";
    private AssetService : AssetService;

  constructor(assetService : AssetService) {
    this.AssetService = assetService;
  }

  ngOnInit() { }

  NavigateTo(path){
    this.AssetService.NavigateTo(path);
  }
}
