import { Injectable } from '@angular/core';
import { Asset } from '../models/Asset.model';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AssetService {
  assets: Asset[];


  constructor(private db: AngularFirestore, public router: Router) { }

  getAssets() : Observable<any> {
   return  this.db.collection('assets').snapshotChanges();
  }
  getAsset(serialNumber: string) {
    return this.db.doc('assets/' + serialNumber ).valueChanges();
  }
  addAsset(asset: Asset) {
    delete asset.id;
    return this.db.collection('assets').add(asset);
  }
  updateAsset(asset: Asset) {
    const assetId = asset.id;
    delete asset.id;
    return this.db.doc('assets/'+ assetId).update(asset);
  }
  NavigateTo(path: string, params?: string) {
    this.router.navigate([path]);
  }
   onDelete(id : string){
    this.db.doc('assets/'+id).delete();
  }
}
