import { Injectable } from '@angular/core';
import { UserDetails } from '../models/user-details.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserDetailService {

  UserDetails : UserDetails[];
  constructor(private db: AngularFirestore, public router:Router) { }

  getUserDetails(): Observable<any>{
    return this.db.collection('UserDetails').snapshotChanges();
  }
  getUserDetail(id: string){
    return this.db.doc('UserDetails/'+id).valueChanges();
  }
}
