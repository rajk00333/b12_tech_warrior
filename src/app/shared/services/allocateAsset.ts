import { Injectable } from '@angular/core';
import { AllocateAsset } from '../models/allocateAsset.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Asset } from '../models/Asset.model';
import { UserDetails } from '../models/user-details.model';
import { AssetService } from './asset.service';
import { UserDetailService } from './user-detail.service';

@Injectable({
    providedIn: 'root'
})

export class allocateAssetService {

    allocatedAsset :  AllocateAsset[];
    assets : Asset[];
    userDetails : UserDetails[];
    AssetService: AssetService;
    UserDetailService : UserDetailService;
    
    constructor(private db: AngularFirestore, public router: Router) {}

    getAllocatedAssets(): Observable<any> {
        return this.db.collection('allocatedAsset').snapshotChanges();
    }
    addAllocatedAsset(allocatedAsset: AllocateAsset) {
        return this.db.collection('allocatedAsset').add(allocatedAsset);
    }

    

}
