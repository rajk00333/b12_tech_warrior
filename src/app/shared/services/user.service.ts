import { Injectable } from '@angular/core';
import {User} from '../models/User.model'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userList : User[] = [{
    email:"raj.kumar@intimetec.com",
    password: "rajkumar"
  }];
  
  constructor() { }

  getUsers(){
    return this.userList;
  }

  addUser(user : User){
  this.userList.push(user);
  }


}
