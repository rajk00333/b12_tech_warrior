// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    // apiKey: "AIzaSyDfan7MUGNGxesBgCntJeyykvEPjDB0xm8",
    // authDomain: "assetmanagement-9c7e7.firebaseapp.com",
    // databaseURL: "https://assetmanagement-9c7e7.firebaseio.com",
    // projectId: "assetmanagement-9c7e7",
    // storageBucket: "assetmanagement-9c7e7.appspot.com",
    // messagingSenderId: "115446359282",
    // appId: "1:115446359282:web:b483f263ad2b4b95"

    apiKey: "AIzaSyAc4B6r7zgv_H8TXNMTZQv8bpN9NR-CupE",
    authDomain: "inventory-e709d.firebaseapp.com",
    databaseURL: "https://inventory-e709d.firebaseio.com",
    projectId: "inventory-e709d",
    storageBucket: "inventory-e709d.appspot.com",
    messagingSenderId: "387396192201",
    appId: "1:387396192201:web:b4054b98b71bd2d83c8a18",
    measurementId: "G-EH26RLCDPR"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
